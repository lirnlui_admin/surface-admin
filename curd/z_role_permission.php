<?php
 return array (
  'table' => 'z_role_permission',
  'title' => '权限组规则表',
  'description' => '权限组规则表',
  'auto_timestamp' => true,
  'button_default' =>
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'page' => true,
  'extend' =>
  array (
  ),
  'pk' => 'id',
  'button' =>
  array (
  ),
  'fields' =>
  array (
    'id' =>
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' =>
      array (
      ),
      'table_type' => 'column',
      'table_format' =>
      array (
      ),
      'table_sort' => false,
      'table_extend' =>
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
'form_extend' =>
      array (
      ),
      'form_extend' =>
      array (
      ),
      'save_format' =>
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' =>
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'role_id' =>
    array (
      'title' => 'role_id',
      'field' => 'role_id',
      'default' => NULL,
      'weight' => 20,
      'search_type' => '_',
      'search' => '=',
      'search_extend' =>
      array (
      ),
      'table_type' => 'column',
      'table_format' =>
      array (
      ),
      'table_sort' => false,
      'table_extend' =>
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
'form_extend' =>
      array (
      ),
      'form_extend' =>
      array (
      ),
      'save_format' =>
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' =>
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'admin_id' =>
    array (
      'title' => 'admin_id',
      'field' => 'admin_id',
      'default' => NULL,
      'weight' => 15,
      'search_type' => '_',
      'search' => '=',
      'search_extend' =>
      array (
      ),
      'table_type' => 'column',
      'table_format' =>
      array (
      ),
      'table_sort' => false,
      'table_extend' =>
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
'form_extend' =>
      array (
      ),
      'form_extend' =>
      array (
      ),
      'save_format' =>
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' =>
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
  ),
);
