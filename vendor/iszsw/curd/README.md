<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">ThinkPHP-CURD</h1>
<h4 align="center">后台开发神器，通过可视化配置生成CURD页面</h4>

## 特性

- 0代码
- SQL注入过滤
- 自定义按钮,样式
- 一键生成访问页面
- 表修改删除事件绑定
- 字段修改删除事件绑定
- 轻松绑定自定义处理逻辑
- 多表关联、一对一、一对多、远程一对多

## 文档
[https://www.kancloud.cn/zswok/think-curd](https://www.kancloud.cn/zswok/think-curd)

## 演示

[http://curd.demo.zsw.ink/](http://curd.demo.zsw.ink/)


## 页面

- 表

[![cZxRxA.png](https://z3.ax1x.com/2021/04/02/cZxRxA.png)](https://z3.ax1x.com/2021/04/02/cZxRxA.png)
[![cZx22d.png](https://z3.ax1x.com/2021/04/02/cZx22d.png)](https://z3.ax1x.com/2021/04/02/cZx22d.png)

- 字段

[![cZxg8H.png](https://z3.ax1x.com/2021/04/02/cZxg8H.png)](https://z3.ax1x.com/2021/04/02/cZxg8H.png)
[![cZxcPe.png](https://z3.ax1x.com/2021/04/02/cZxcPe.png)](https://z3.ax1x.com/2021/04/02/cZxcPe.png)

- 页面

[![cZxy5D.png](https://z3.ax1x.com/2021/04/02/cZxy5D.png)](https://z3.ax1x.com/2021/04/02/cZxy5D.png)


## 安装

> 运行环境要求PHP7.1+。

```shell
$ composer require iszsw/curd
```

## 使用

> 1、连接数据库

> 2、访问 http://domain.com/curd （路由前缀配置查看config/curd.php配置文件route_prefix）就是这么直接


## 说明

页面基于PHP页面生成工具 [iszsw/surface](https://gitee.com/iszsw/surface) 

如果功能不够用可以方便的快速的使用surface生成Table、Form页面

## 介绍

> **邮件** zswemail@qqcom

> **主页**  [https://zsw.ink](https://zsw.ink) 留言

> **github**  [https://github.com/iszsw/curd](https://github.com/iszsw/curd)

> **gitee**  [https://gitee.com/iszsw/curd](https://gitee.com/iszsw/curd)
