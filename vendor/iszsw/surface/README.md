<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Surface</h1>
<h4 align="center">快速构建后台页面，PHP页面构建器</h4>

## 生态

thinkPHP6 **0代码**生成CURD页面 [iszsw/curd](https://gitee.com/iszsw/curd)

## 文档
[https://www.kancloud.cn/zswok/surface/](https://www.kancloud.cn/zswok/surface/)

## 演示

[http://surface.demo.zsw.ink/](http://surface.demo.zsw.ink/)

[查看文档](https://www.kancloud.cn/zswok/surface)

## 特性

- 面向对象风格
- 自定义主题样式
- 动态生成Form页面
- 动态生成Table页面
- 丰富的表格表单样式
- 高扩展性，快速友好的扩展自定义的组件样式

## 页面

> Form 组件

![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jcp11.png)

> Table组件

![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jcSpR.png)
![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jc96x.png)


## 安装

> 运行环境要求PHP7.1+。

```shell
$ composer require iszsw/surface
```

## 快速使用 演示代码

[演示 /test/index.php](/test/index.php) 

[助手函数演示 /test/helper.php](/test/helper.php) 


## Test

```shell
# 下载代码 进入目录

$ composer install

$ cd test

$ php -S localhost:888
```

访问
[localhost:888](http://localhost:888) 

访问助手
[localhost:888/helper.php](http://localhost:888/helper.php) 

## 关于

> **邮件** zswemail@qq.com
>
> **主页**  [https://zsw.ink](https://zsw.ink) 留言

> **文档**  [https://doc.zsw.ink/surface/](https://doc.zsw.ink/surface/) 

> **github**  [https://github.com/iszsw/surface](https://github.com/iszsw/surface)

> **gitee**  [https://gitee.com/iszsw/surface](https://gitee.com/iszsw/surface)


## 说明

前端基于 [iszsw/surface-src](https://gitee.com/iszsw/surface-src) 

如果需要自定义后端开发 可以自行引入前端逻辑
