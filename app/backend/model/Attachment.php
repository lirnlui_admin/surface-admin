<?php

namespace app\backend\model;

class Attachment extends Base
{

    public static $search = [
        'ip' => '=',
        'name' => 'LIKE',
        'url' => 'LIKE',
        'size' => 'BETWEEN',
        'create_time' => 'BETWEEN',
    ];

    public static $labels = [
        'id'                => 'ID',
        'uuid'              => 'UUID',
        'uploader'          => '上传者',
        'name'              => '文件名',
        'img'               => '图片',
        'url'               => '地址',
        'ip'                => '上传IP',
        'mime'              => '文件类型',
        'size'              => '大小',
        'wh'                => '尺寸(px)',
        'create_time'       => '上传时间',
        'update_time'       => '更新时间',
        'width'             => '宽度',
        'height'            => '高度',
        'suffix'            => '后缀',
    ];

    public static function getImage($uuid)
    {
        return self::where('uuid', $uuid)->value('url');
    }

    protected function setIpAttr($ip)
    {
        return ip2long($ip);
    }

    protected function getIpAttr($ip)
    {
        return long2ip($ip);
    }

    public static function onBeforeInsert($model)
    {
        if (self::where('url', '=', $model['url'])->find()) {
            return false;
        }
    }

}
