<?php
declare (strict_types = 1);

namespace app\backend\services\auth;


/**
 * 应用服务类
 */
class Service extends \think\Service
{
    /**
     * @var Authority
     */
    private $auth;

    public function register()
    {
        $this->auth = $this->app->invokeClass(Authority::class);

        $menus = include app_path('backend') . 'menu.php';
        $this->auth->addPermissions($menus);

        $this->app->bind('auth', $this->auth);
    }

}
