<?php

namespace app\backend\controller\attachment;

use app\backend\model\Attachment as AttachmentModel;
use surface\form\components\Date;
use surface\form\components\Input;
use surface\form\components\Slider;
use surface\helper\AbstractForm;

class Search extends AbstractForm
{
    public function init($form)
    {
        $form->search(true);
    }

    public function options(): array
    {
        return [
            'submitBtn' => [
                'props' => [
                    'prop' => [
                        'icon' => 'el-icon-search',
                    ],
                ]
            ]
        ];
    }

    public function columns(): array
    {
        $min = AttachmentModel::min('size');
        $max = AttachmentModel::max('size');
        return [
            new Input('uploader', AttachmentModel::$labels['uploader']),
            new Input('name', AttachmentModel::$labels['name']),
            new Input('url', AttachmentModel::$labels['url']),
            (new Date('create_time', AttachmentModel::$labels['create_time'] . 'B'))->props(
                [
                    'type'        => "datetimerange",
                    'value-format' => "yyyy-MM-dd HH:mm:ss",
                    'placeholder' => "请选择活动日期",
                ]
            ),
            (new Slider('size', AttachmentModel::$labels['size'], [$min, $max]))->props(['min' => $min, 'max' => $max, 'range' =>true])
        ];
    }

}
