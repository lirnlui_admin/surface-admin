<?php

namespace app\backend\controller\attachment;

use app\backend\logic\Attachment as AttachmentLogic;
use app\backend\model\Attachment as AttachmentModel;
use surface\Component;
use surface\helper\AbstractTable;
use surface\helper\Condition;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Selection;

class Table extends AbstractTable
{
    use Condition;

    private $rulePrefix = '/backend/attachment';

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix.'/delete'],
                    '确认删除选中列',
                    'id'
                )->props('doneRefresh', true),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createPage($this->rulePrefix.'/search')->props('doneRefresh', false),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', AttachmentModel::$labels['id'])),
            (new Column('id', AttachmentModel::$labels['id']))->props('width', '50px'),
            (new Column('uploader_name', AttachmentModel::$labels['uploader']))->props(['width' => '100px', 'show-overflow-tooltip' => true]),
            (new Column('url', AttachmentModel::$labels['img']))->scopedSlots(
                [
                    (new Component())->el('el-image')->inject('attrs', ['src', 'array.preview-src-list'])->style('width', '100px'),
                ]
            )->props('width', '100px'),
            (new Column('name', AttachmentModel::$labels['name'])),
            (new Column('ip', AttachmentModel::$labels['ip']))->props('width', '120px'),
            (new Column('size_label', AttachmentModel::$labels['size']))->props('width', '60px'),
            (new Column('wh', AttachmentModel::$labels['wh']))->props('width', '110px'),
            (new Column('mime', AttachmentModel::$labels['mime']))->props('width', '110px'),
            (new Column('suffix', AttachmentModel::$labels['suffix']))->props('width', '60px'),
            (new Column('create_time', AttachmentModel::$labels['create_time']))->props('width', '100px'),

            (new Column('options', '操作'))->props('fixed', 'right')
                ->scopedSlots(
                    [
                        (new Button('el-icon-delete', '删除'))
                            ->visible('_delete_visible')
                            ->props(['doneRefresh' => true])
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix . '/delete']),
                    ]
                )->props('width', '80px'),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        $where = array_filter($where);
        $conditions = [];
        foreach ($where as $k => $v)
        {
            if ($k == 'create_time') {
                foreach ($v as &$x) {
                    $x = strtotime($x);
                }
                unset($x);
            }
            $conditions[] = $this->condition(AttachmentModel::$search[$k] ?? '=', $k, $v);
        }

        return AttachmentLogic::tableList($conditions, $order, $page, $limit);
    }


}
