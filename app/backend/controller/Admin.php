<?php

namespace app\backend\controller;

use app\backend\controller\admin\
{Form, Search, Table};
use app\backend\model\Admin as AdminModel;


class Admin extends BackendController
{

    /**
     * 搜索
     *
     * @param Search $search
     *
     * @return array|string
     */
    public function search(Search $search)
    {
        return $this->createForm($search);
    }

    public function index(Table $table)
    {
        return $this->createTable($table);
    }

    public function create(Form $form)
    {
        return $this->update($form);
    }

    public function update(Form $form)
    {
        return $this->createForm($form);
    }

    /**
     * @param $id
     * @param $field
     * @param $value
     *
     * @return \think\response\Json|\think\response\View
     */
    public function change($id, $field, $value, AdminModel $admin)
    {
        if ($id === AdminModel::ADMIN)
        {
            return _error('不能操作超级管理员');
        }
        if (! in_array($field, ['nickname', 'status']))
        {
            return _error('No data submission');
        }
        $admin->editField($field, $value, $id);

        return _success(['edit', 'success']);
    }

    /**
     * @param $id
     *
     * @return \think\response\Json|\think\response\View
     */
    public function delete($id)
    {
        if (
            is_array($id) && in_array(AdminModel::ADMIN, $id) ||
            is_numeric($id) && AdminModel::ADMIN === $id
        ) {
            return _error('不能操作超级管理员');
        }

        AdminModel::destroy($id);

        return _success(['delete', 'success']);
    }
}
