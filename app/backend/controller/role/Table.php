<?php

namespace app\backend\controller\role;

use app\backend\model\Role as RoleModel;
use surface\Component;
use surface\helper\AbstractTable;
use surface\helper\Condition;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Selection;
use surface\table\components\Switcher;
use surface\table\components\Writable;

class Table extends AbstractTable
{
    use Condition;

    private $rulePrefix = '/backend/role';

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'post', 'data' => ['id'], 'url' => $this->rulePrefix . '/delete'],
                    '确认删除选中列',
                    'id'
                ),
                (new Button('el-icon-plus', '增加'))->createPage($this->rulePrefix .  '/create'),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createPage($this->rulePrefix . '/search')->props('doneRefresh', false),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', RoleModel::$labels['id'])),
            (new Column('id', RoleModel::$labels['id'])),
            (new Column('title', RoleModel::$labels['title']))->scopedSlots(
                [
                    (new Writable())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('status', RoleModel::$labels['status']))->scopedSlots(
                [
                    (new Switcher())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('options', '操作'))->props('fixed', 'right')
                ->scopedSlots(
                    [
                        (new Button('el-icon-set-up', '分派权限'))->createPage($this->rulePrefix . '/assigns', ['id']),
                        (new Button('el-icon-edit', '编辑'))->createPage($this->rulePrefix . '/update', ['id'])->props('doneRefresh', true),
                        (new Button('el-icon-delete', '删除'))
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix]),
                    ]
                ),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        $where = array_filter($where);
        $conditions = [];
        foreach ($where as $k => $v) {
            $conditions[] = $this->condition(RoleModel::$search[$k] ?? '=', $k, $v);
        }

        $model = (new RoleModel())->where($where);
        return [
            'count' => $model->count(),
            'list' => $model->order($order)->page($page, $limit)->select()
        ];
    }

}
