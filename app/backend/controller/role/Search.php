<?php

namespace app\backend\controller\role;

use app\backend\model\Role as RoleModel;
use surface\form\components\Input;
use surface\helper\AbstractForm;

class Search extends AbstractForm
{
    public function init($form)
    {
        $form->search(true);
    }

    public function options(): array
    {
        return [
            'submitBtn' => [
                'props' => [
                    'prop' => [
                        'icon' => 'el-icon-search',
                    ],
                ]
            ]
        ];
    }

    public function columns(): array
    {
        return [
            new Input('title', RoleModel::$labels['title'])
        ];
    }

}
