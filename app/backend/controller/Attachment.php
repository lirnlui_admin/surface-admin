<?php

namespace app\backend\controller;

use app\backend\controller\attachment\Search;
use app\backend\controller\attachment\Table;
use app\backend\logic\Attachment as AttachmentLogic;
use app\backend\logic\Uploader;
use think\facade\View;

class Attachment extends BackendController
{

    public function index(Table $table)
    {
        return $this->createTable($table);
    }


    public function search(Search $search)
    {
        return $this->createForm($search);
    }


    public function delete($id)
    {
        if(AttachmentLogic::delete($id)){
            return _success('删除成功');
        }
        return _error('删除失败');
    }

    public function manage()
    {
        return View::fetch('', [
            'ext'    => ['png','gif','jpg','jpeg','bmp'],
        ]);
    }

    /**
     * 文件上传
     *
     * 文件 | base64
     *
     */
    public function upload(){
        try {
            if (input('base64')) {
                $file = input('img_base64_data');
            }else{
                $file = request()->file('file');
            }
            $fileType = input('fileType', 'image');
            $url = (new Uploader)->handle($file, [
                'uploader' => session('admin.id'),
            ], $fileType);
        } catch (\Exception $e){
            return _error($e->getMessage());
        }
        return _success('上传成功', ['url' => $url]);
    }

}
