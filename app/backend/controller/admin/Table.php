<?php

namespace app\backend\controller\admin;

use app\backend\logic\Admin as AdminLogic;
use app\backend\model\Admin as AdminModel;
use surface\Component;
use surface\helper\AbstractTable;
use surface\helper\Condition;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Selection;
use surface\table\components\Switcher;
use surface\table\components\Writable;

class Table extends AbstractTable
{
    use Condition;

    private $rulePrefix = '/backend/admin';

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix . '/delete'],
                    '确认删除选中列',
                    'id'
                ),
                (new Button('el-icon-plus', '增加'))->createPage($this->rulePrefix .  '/create'),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createPage($this->rulePrefix . '/search')->props('doneRefresh', false),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', AdminModel::$labels['id'])),
            (new Column('id', AdminModel::$labels['id']))->props(['width' => '60px', 'sortable' => true]),
            (new Column('avatar', AdminModel::$labels['avatar']))->scopedSlots(
                [
                    (new Component())->el('el-image')->inject('attrs', ['src', 'array.preview-src-list'])->style(['width' => '50px', 'borderRadius'=>'50px']),
                ]
            )->props('width', '70px'),
            (new Column('username', AdminModel::$labels['username'])),
            (new Column('nickname', AdminModel::$labels['nickname']))->scopedSlots(
                [
                    (new Writable())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('status', AdminModel::$labels['status']))->scopedSlots(
                [
                    (new Switcher())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('create_time', AdminModel::$labels['create_time'])),
            (new Column('role', AdminModel::$labels['roles'])),
            (new Column('options', '操作'))->props('fixed', 'right')
                ->scopedSlots(
                    [
                        (new Button('el-icon-edit', '编辑'))
                            ->visible('_edit_visible')
                            ->createPage($this->rulePrefix . '/update', ['id'])->props('doneRefresh', true),
                        (new Button('el-icon-delete', '删除'))
                            ->visible('_delete_visible')
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix]),
                    ]
                ),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        $where = array_filter($where);
        $conditions = [];
        foreach ($where as $k => $v) {
            if ($k == 'create_time') {
                foreach ($v as &$x) {
                    $x = strtotime($x);
                }
                unset($x);
            }
            $conditions[] = $this->condition(AdminModel::$search[$k] ?? '=', $k, $v);
        }

        return AdminLogic::tableList($conditions, $order, $page, $limit);
    }

}
