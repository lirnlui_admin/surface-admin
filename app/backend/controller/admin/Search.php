<?php

namespace app\backend\controller\admin;

use app\backend\model\Admin as AdminModel;
use surface\form\components\Date;
use surface\form\components\Input;
use surface\helper\AbstractForm;

class Search extends AbstractForm
{
    public function init($form)
    {
        $form->search(true);
    }

    public function options(): array
    {
        return [
            'submitBtn' => [
                'props' => [
                    'prop' => [
                        'icon' => 'el-icon-search',
                    ],
                ]
            ]
        ];
    }

    public function columns(): array
    {
        return [
            new Input('id', AdminModel::$labels['id']),
            new Input('username', AdminModel::$labels['username']),
            (new Date('create_time', AdminModel::$labels['create_time']))->props(
                [
                    'type'        => "datetimerange",
                    'value-format' => "yyyy-MM-dd HH:mm:ss",
                    'placeholder' => "请选择活动日期",
                ]
            )
        ];
    }

}
