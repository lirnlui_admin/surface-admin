/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : surface_admin

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 20/06/2021 15:36:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for z_admin
-- ----------------------------
DROP TABLE IF EXISTS `z_admin`;
CREATE TABLE `z_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '账号状态 0封号 1正常',
  `update_time` int(11) NOT NULL DEFAULT 0,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of z_admin
-- ----------------------------
INSERT INTO `z_admin` VALUES (1, 'admin', 'admin', '$2y$10$UybQERZCiMdchq1JEOi6tObvKqa69L.O9IA9m6xf0J4iJC2jwUo56', 1562810827, '1', 1605865528, '');
INSERT INTO `z_admin` VALUES (2, 'lalala', '啦啦啦', '$2y$10$rVREDcmjzciyJ2.V.L7Vx.F4MNhTGeju6.vjtkN49AdnI0F6QpPQW', 1535080343, '1', 1624150540, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg');
INSERT INTO `z_admin` VALUES (3, 'zhangsan', '张二狗', '$2y$10$44GoYegX3jdQgXLFfpjcMulxmha9oSFVvPBj2qervmMNPaTEaxmm6', 1535080343, '1', 1624093833, '');

-- ----------------------------
-- Table structure for z_attachment
-- ----------------------------
DROP TABLE IF EXISTS `z_attachment`;
CREATE TABLE `z_attachment`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uuid` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'filehash',
  `uploader` int(10) NOT NULL DEFAULT 0 COMMENT '上传者',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `mime` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建日期',
  `ip` int(10) NOT NULL DEFAULT 0 COMMENT '上传者IP',
  `width` int(11) NOT NULL COMMENT '宽度',
  `height` int(11) NOT NULL COMMENT '高度',
  `suffix` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片类型',
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '域名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of z_attachment
-- ----------------------------
INSERT INTO `z_attachment` VALUES (5, 'bc7698478f5c416de6e26a96d497e09fa54ea08a', 0, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg', 'avatar.jpg', 1208, 'image/jpeg', 1624113532, 2130706433, 300, 300, 'jpg', '');
INSERT INTO `z_attachment` VALUES (6, '00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa', 1, '/uploads/20210620/00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa.jpg', 'no_goods.jpg', 13824, 'image/jpeg', 1624169413, 2130706433, 200, 200, 'jpg', '');
INSERT INTO `z_attachment` VALUES (7, 'b7552a703dd0d899d7886d124dafdb8e8dbd8d84', 1, '/uploads/20210620/b7552a703dd0d899d7886d124dafdb8e8dbd8d84.png', '1.png', 69209, 'image/png', 1624169922, 2130706433, 1016, 267, 'png', '');

-- ----------------------------
-- Table structure for z_post
-- ----------------------------
DROP TABLE IF EXISTS `z_post`;
CREATE TABLE `z_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `sort` smallint(4) NOT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '发布时间',
  `update_time` int(10) NOT NULL COMMENT '修改时间',
  `user_id` int(11) NOT NULL COMMENT '发布者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post
-- ----------------------------
INSERT INTO `z_post` VALUES (7, '啦啦啦', '啦啦啦啦啦啦', '## 哈哈哈哈', 0, 0, 1584282332, 1599097716, 1);
INSERT INTO `z_post` VALUES (8, '啦啦啦', '啦啦啦啦啦啦', '## 哈哈哈哈', 0, 0, 1584282332, 1599097716, 1);
INSERT INTO `z_post` VALUES (9, '啦啦啦', '啦啦啦啦啦啦', '## 哈哈哈哈', 0, 0, 1584282332, 1599097716, 1);
INSERT INTO `z_post` VALUES (18, '啦啦啦', '啦啦啦啦啦啦', '<p>## 哈哈哈哈</p>', 0, 1, 1584282332, 1624174289, 1);
INSERT INTO `z_post` VALUES (20, '啦啦啦啦啦啦啦啦啦啦啦啦', '啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦hello', '<p>## 哈哈哈哈</p><p><img src=\"/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg\" style=\"max-width:100%;\"/><img src=\"/uploads/20210620/b7552a703dd0d899d7886d124dafdb8e8dbd8d84.png\" style=\"max-width: 100%;\"/><br/></p><p><img src=\"/uploads/20210620/00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa.jpg\" style=\"max-width:50%;\"/><br/></p>', 0, 1, 1584282332, 1624169931, 1);

-- ----------------------------
-- Table structure for z_post_tag
-- ----------------------------
DROP TABLE IF EXISTS `z_post_tag`;
CREATE TABLE `z_post_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL COMMENT '文章',
  `tag_id` int(50) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post_tag
-- ----------------------------
INSERT INTO `z_post_tag` VALUES (1, 1, 1);
INSERT INTO `z_post_tag` VALUES (2, 20, 1);
INSERT INTO `z_post_tag` VALUES (4, 18, 1);
INSERT INTO `z_post_tag` VALUES (5, 20, 3);

-- ----------------------------
-- Table structure for z_role
-- ----------------------------
DROP TABLE IF EXISTS `z_role`;
CREATE TABLE `z_role`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限规则ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of z_role
-- ----------------------------
INSERT INTO `z_role` VALUES (1, '测试', 1, '');

-- ----------------------------
-- Table structure for z_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `z_role_permission`;
CREATE TABLE `z_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` mediumint(8) UNSIGNED NOT NULL,
  `role_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_group_id`(`admin_id`, `role_id`) USING BTREE,
  INDEX `uid`(`admin_id`) USING BTREE,
  INDEX `group_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组规则表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of z_role_permission
-- ----------------------------
INSERT INTO `z_role_permission` VALUES (1, 1, 1);
INSERT INTO `z_role_permission` VALUES (9, 2, 1);
INSERT INTO `z_role_permission` VALUES (2, 3, 1);

-- ----------------------------
-- Table structure for z_tags
-- ----------------------------
DROP TABLE IF EXISTS `z_tags`;
CREATE TABLE `z_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `create_time` int(10) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_tags
-- ----------------------------
INSERT INTO `z_tags` VALUES (1, 'PHP', 1584282332);
INSERT INTO `z_tags` VALUES (3, '啦啦', 1624174412);

-- ----------------------------
-- Table structure for z_user
-- ----------------------------
DROP TABLE IF EXISTS `z_user`;
CREATE TABLE `z_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `age` tinyint(3) UNSIGNED NOT NULL DEFAULT 18 COMMENT '年龄',
  `sex` enum('1','2') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '性别',
  `status` enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '加入时间',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_user
-- ----------------------------
INSERT INTO `z_user` VALUES (1, '啦啦啦', '/uploads/20210620/00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa.jpg', 18, '1', '1', 1624167027, '$2y$10$.i8jhwwuRnzg0sRKdINsXu6rXOOH3H0WUpnKfV35oBXAzIA9AEK/2');

SET FOREIGN_KEY_CHECKS = 1;
